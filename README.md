# Interview Challenge 2
You have 24 hours from the receipt of this document to:

- Write the code to complete the challenge
- Push it up to a repository in [GitHub][3] or [BitBucket][4]
- Send us the url to the repository.

We expect to clone the repository, examine, and run your working code.


## The Challenge
Create a java based [Selenium WebDriver][0] test that will verify that the
following four modules are visible on [http://www.cnn.com][1] utilizing
[ChromeDriver Server][2].

- The Header

    ![](http://grab.by/llzQ)


- The Footer

    ![](http://grab.by/llES)


- The Weather module

    ![](http://grab.by/llAg)


- The Markets module

    ![](http://grab.by/llAm)

[0]: http://seleniumhq.org
[1]: http://www.cnn.com
[2]: https://code.google.com/p/chromedriver/downloads/list
[3]: http://github.com
[4]: http://bitbucket.org
